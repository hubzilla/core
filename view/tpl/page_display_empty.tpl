{{$body}}
{{if $edit_link}}
<div class="position-fixed bottom-0 end-0 m-3">
	<a href="{{$edit_link}}" class="btn btn-lg btn-primary rounded-circle z-1"><i class="bi bi-pencil"></i></a>
</div>
{{/if}}
